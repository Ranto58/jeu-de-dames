from tkinter import *
import time
from src.pion import Pion


dim = 666
size = dim/10


master = Tk()
game = Canvas(master, width=dim, height=dim)
game.pack()
pions = []
team = "red"
highlight = [12,12]
possibleMove = []
for i in range(10):
        for j in range(4):
            if((i+j)%2 == 0):
                pion = Pion(game, i, j, size, team)
                pions.append(pion)
team = "green"
for i in range(10):
    for j in range(4):
        if((i+j)%2 == 1):
            pion = Pion(game, i, 9 - j, size, team)
            pions.append(pion)
            
def isPion(x, y):
    for i in range(len(pions)):
        if((pions[i].x == x) & (pions[i].y == y)):
            return pions[i]
    return False
def isPionIndex(x, y):
    for i in range(len(pions)):
        if((pions[i].x == x) & (pions[i].y == y)):
            return i
    return False
def deselect():
    for i in range(len(pions)):
        pions[i].border = 1

def possibleMoves(pion):
    #Verifier avant gauche
    direction = -1
    if(pion.team == "red"):
        direction = 1
    if(not isPion( pion.x - 1, pion.y + direction)):
        possibleMove.append([pion.x - 1, pion.y + direction, False])
    else:
        if(not isPion( pion.x - 2, pion.y + direction*2)):
            index = isPionIndex(pion.x - 1, pion.y + direction*1)
            possibleMove.append([pion.x - 2, pion.y + direction*2, index])
    if(not isPion( pion.x + 1, pion.y + direction)):
        possibleMove.append([pion.x + 1, pion.y + direction, False])
    else:
        if(not isPion( pion.x + 2, pion.y + direction*2)):
            index = isPionIndex(pion.x + 1, pion.y + direction*1)
            possibleMove.append([pion.x + 2, pion.y + direction*2, index])
        

def click(event):
    #Detecter si ya des moves possibles
    for move in possibleMove:
        if((int(event.x // size) == move[0]) & (int(event.y // size) == move[1])):
            pion = isPion(highlight[0],highlight[1])
            if(move[2]):
                pions[move[2]].die()
            pion.x = event.x // size
            pion.y = event.y // size
    
    highlight[0] = int(event.x // size)
    highlight[1] = int(event.y // size)
    
    
            
    
    possibleMove.clear()
    deselect()
    #Detecter si on est sur un pion
    pion = isPion(highlight[0], highlight[1])
    if(pion):
        possibleMoves(pion)
        pion.border = 4

        
while True:
    
    for i in range(10):
        for j in range(10):
            color = "#E9D2B2"
            if (i+j)%2 == 0:
                color = "#A37A5A"
            
            #if (i%2 == 0) & (j%2 == 0):
            #    color = "red"
            #if (i%2 == 1) & (j%2 == 1):
            #    color = "green"
            game.create_rectangle(0 + size*i, 0+size*j, size + size*i , size + size*j, fill=color, outline = 'black')
    # Montrer Highlight
    game.create_rectangle(0 + size*highlight[0], 0+size*highlight[1], size + size*highlight[0] , size + size*highlight[1], fill="#44e")

    #Montrer possibleMove
    for move in possibleMove:
        game.create_rectangle(0 + size*move[0], 0+size*move[1], size + size*move[0] , size + size*move[1], fill="cyan")
    for pion in pions:
        pion.draw()
    master.update()
    game.delete("all")
    game.bind("<Button-1>", click)
    time.sleep(0.01)    





	

	

master.mainloop()


    
