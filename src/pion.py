from tkinter import *

class Pion:
    def __init__(cls, game, x, y, size, team):
        cls.x = x
        cls.y = y
        cls.game = game
        cls.team = team
        cls.size = size
        cls.radius = size / 3
        cls.border = 1
        cls.isAlive = True
    def draw(cls):
        if(cls.isAlive):
            cls.game.create_oval(cls.size * cls.x + cls.radius + cls.size / 2 ,
                                 cls.size * cls.y + cls.radius + cls.size / 2,
                                 cls.size * cls.x - cls.radius + cls.size / 2,
                                 cls.size * cls.y - cls.radius + cls.size / 2,
                                 fill=cls.team,
                                 outline="white",
                                 width=cls.border)
    def die(cls):
        cls.x = 11
        cls.y = 11
        cls.isAlive = False
